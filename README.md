# World Bank Takehome

## Local Development

```bash
npm ci
npm start
```

## Dev Considerations

- I would prefer to store the country list "in memory" as a config file, rather than fetching the data each time the app loads. This data is _unikely_ to change with any frequency and because it's critical to the application functioning, I want to have that data available as fast as possible.
- Use Redux to show its power over Context API, as well as some of the baggage that comes with it. I have recently been using the easy-peasy package as a happy medium for this.
- With more time I would like to make the charts more responsive and functional. Currently they are rendered with static height and widths.

## Future Enhancements

- Transform the Rednered Chart component into a HOC to improve scalability
- Implement React Testing LIbrary
- Implement dark/light mode transformation (basic features exist in code)
