import React, { useState } from "react";
import { Provider } from "react-redux";
import styled from "@emotion/styled";
import { ThemeProvider } from "@emotion/react";

import store from "./store";
import RootContainer from "./RootContainer";

const AppWrapper = styled.div`
  // width: 80%;
  // margin: 0 auto 0 auto;
  & h1,
  h2 {
    font-family: "Roboto", sans-serif !important;
  }
`;

const themeLight = {
  tex: "#000",
  background: "#fff",
  buttonText: "#000",
  buttonTextHover: "#fff",
  buttonBorder: "#000",
  button: "#fff",
};

const themeDark = {
  text: "#fff",
  background: "#121212",
  buttonText: "#fff",
  buttonTextHover: "#000",
  buttonBorder: "2px solid #fff",
  buttonBg: "#121212",
  buttonBgHover: "#fff",
  layerTwo: "#212121",
  layerThree: "#323232",
};

function App() {
  const [isDark, setIsDark] = useState(true);

  return (
    <Provider store={store}>
      <ThemeProvider theme={isDark ? themeDark : themeLight}>
        <AppWrapper>
          <RootContainer isDark={isDark} setIsDark={setIsDark} />
        </AppWrapper>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
