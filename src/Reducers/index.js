import * as CountryReducer from "./CountryReducer";
import { combineReducers } from "redux";

const reducers = combineReducers(Object.assign(CountryReducer));

export default reducers;
