const defaultState = {
  error: false,
  countryList: null,
  fetching: false,
  selectedCountry: null,
  grossDomesticData: null,
  temperatureData: null,
  currentChartView: "GDP",
};

export const CountryReducer = (state = defaultState, action) => {
  switch (action.type) {
    /**
     * GET_COUNTRIES
     */
    case "GET_COUNTRIES_REQUEST": {
      return {
        ...state,
        fetching: true,
      };
    }

    case "GET_COUNTRIES_SUCCESS": {
      return {
        ...state,
        fetching: false,
        countryList: action.payload,
      };
    }

    case "GET_COUNTRIES_FAILURE": {
      return {
        ...state,
        fetching: false,
        error: true,
      };
    }

    /**
     * GET_GDP
     */
    case "GET_GDP_REQUEST": {
      return {
        ...state,
        fetching: true,
      };
    }

    case "GET_GDP_SUCCESS": {
      console.log("THE ACTION PAYLOAD IS", action.payload);
      return {
        ...state,
        fetching: false,
        grossDomesticData: action.payload,
      };
    }

    case "GET_GDP_FAILURE": {
      return {
        ...state,
        fetching: false,
        error: true,
      };
    }

    /**
     * GET_TEMPERATURE
     */
    case "GET_TEMPERATURE_REQUEST": {
      return {
        ...state,
        fetching: true,
      };
    }

    case "GET_TEMPERATURE_SUCCESS": {
      console.log("THE ACTION PAYLOAD IS", action.payload);
      return {
        ...state,
        fetching: false,
        temperatureData: action.payload,
      };
    }

    case "GET_TEMPERATURE_FAILURE": {
      return {
        ...state,
        fetching: false,
        error: true,
      };
    }

    /**
     * SET_COUNTRY APP ACTION
     */
    case "SET_COUNTRY": {
      return {
        ...state,
        selectedCountry: action.payload,
        grossDomesticData: null,
        temperatureData: null,
      };
    }

    /**
     * SET_CHART_VIEW APP ACTION
     */
    case "SET_CHART_VIEW": {
      return {
        ...state,
        currentChartView: action.payload,
      };
    }

    default:
      return state;
  }
};
