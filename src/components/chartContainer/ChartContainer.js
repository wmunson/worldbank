import React from "react";
import { connect } from "react-redux";

import Actions from "../../Actions";
import ChartSelector from "./chartSelector/ChartSelector";
import RenderedChart from "./renderedChart/RenderedChart";

const ChartContainer = (props) => {
  const chartOptions = {
    GDP: {
      displayName: "Gross Domestic Product",
      dataSource: "grossDomesticData",
      xAxisKey: "date",
      yAxisKey: "value",
      countryCode: props.selectedCountry,
    },
    TEMP: {
      displayName: "Temperature",
      dataSource: "temperatureData",
      xAxisKey: "year",
      yAxisKey: "data",
      countryCode: props.selectedCountry,
    },
  };

  return (
    <>
      <ChartSelector />
      <RenderedChart settings={chartOptions[props.currentChartView]} />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    selectedCountry: state.CountryReducer.selectedCountry,
    currentChartView: state.CountryReducer.currentChartView,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGDP: (country) => {
      dispatch(Actions.getGDP(country));
    },
    getTemperature: (country) => {
      dispatch(Actions.getTemperature(country));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChartContainer);
