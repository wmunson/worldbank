import React from "react";
import styled from "@emotion/styled";
import { connect } from "react-redux";
import { useTheme } from "@emotion/react";

import GDPChart from "./gdpChart/GDPChart";
import TemperatureChart from "./temperatureChart/TemperatureChart";

const RenderedChartWrapper = styled.div`
  margin: 0 auto 0 auto;
  padding: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${(props) => props.theme.layerThree};

  border: 1px solid black;

  & .recharts-wrapper {
    margin: 0 auto 0 auto;
  }

  & h2 {
    text-align: center;
  }

  & h3 {
    text-align: center;
    color: white;
  }

  & i {
    text-align: center;
  }
`;

const RenderedChart = (props) => {
  const { currentChartView } = props;
  const theme = useTheme();

  // return loader when fetching
  if (props.fetching) {
    return <h3>Data Loading</h3>;
  }

  if (currentChartView === "GDP") {
    return (
      <RenderedChartWrapper theme={theme}>
        <GDPChart />
      </RenderedChartWrapper>
    );
  } else if (currentChartView === "TEMP") {
    return (
      <RenderedChartWrapper>
        <TemperatureChart />
      </RenderedChartWrapper>
    );
  }

  return <h1>null</h1>;
};

const mapStateToProps = (state) => {
  return {
    fetching: state.CountryReducer.fetching,
    currentChartView: state.CountryReducer.currentChartView,
  };
};

export default connect(mapStateToProps)(RenderedChart);
