import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { LineChart, Line, XAxis, YAxis, Tooltip } from "recharts";

import Actions from "../../../../Actions";

const TemperatureChart = (props) => {
  const { fetching, temperatureData, selectedCountry } = props;
  const [callFired, setCallFired] = useState(false);

  useEffect(() => {
    if (!temperatureData && !callFired) {
      setCallFired(true);
      props.getTemperature(selectedCountry.value);
    }
  }, [temperatureData, callFired, props, selectedCountry.value]);

  if (fetching || !temperatureData) {
    return <h1>Fetching</h1>;
  }

  return (
    <>
      <h2>Average Temperature</h2>
      <i>In degrees celcius</i>
      <LineChart width={600} height={300} data={temperatureData}>
        <XAxis dataKey="year" />
        <YAxis />
        <Line dataKey="data" />
        <Tooltip />
      </LineChart>
      {/* </RenderedChartWrapper> */}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTemperature: (countryCode) => {
      dispatch(Actions.getTemperature(countryCode));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    temperatureData: state.CountryReducer.temperatureData,
    selectedCountry: state.CountryReducer.selectedCountry,
    fetching: state.CountryReducer.fetching,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TemperatureChart);
