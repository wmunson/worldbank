import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { LineChart, Line, XAxis, YAxis, Tooltip } from "recharts";

import Actions from "../../../../Actions";

const GDPChart = (props) => {
  const { fetching, grossDomesticData, selectedCountry } = props;
  const [got, setGot] = useState(false);

  useEffect(() => {
    if (!grossDomesticData && !got) {
      setGot(true);
      props.getGDP(selectedCountry.value);
    }
  }, [grossDomesticData, got, props, selectedCountry.value]);

  if (fetching || !grossDomesticData) {
    return <h1>Fetching</h1>;
  }

  // format data for the chart
  let chartData = [];
  for (let i of grossDomesticData) {
    if (i.value) {
      chartData.push({
        value: Math.floor(i.value * 0.000001),
        date: i.date,
      });
    }
  }

  return (
    <>
      <h2>Gross Domestic Product</h2>
      <i>In millions $USD</i>
      <LineChart width={600} height={300} data={chartData.reverse()}>
        <XAxis dataKey="date" />
        <YAxis />
        <Line dataKey="value" />
        <Tooltip />
      </LineChart>
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGDP: (countryCode) => {
      dispatch(Actions.getGDP(countryCode));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    grossDomesticData: state.CountryReducer.grossDomesticData,
    selectedCountry: state.CountryReducer.selectedCountry,
    fetching: state.CountryReducer.fetching,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GDPChart);
