import React from "react";
import styled from "@emotion/styled";
import { connect } from "react-redux";
import { useTheme } from "@emotion/react";

import Actions from "../../../Actions";
import CountrySelect from "../../countrySelect/CountrySelect";

const ChartSelectorWrapper = styled.div`
  padding: 0.5rem 0.5rem 0.5rem 1rem;
  margin: 1rem auto 1rem;
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  background-color: ${(props) => props.theme.layerTwo};

  & .countryDropdown {
    width: 11rem !important;
  }

  & .countryNameDiv {
    display: flex;
    flex-direction: row;
    align-items: baseline;
    width: 12rem;

    & button {
      margin-left: 0.5rem;
    }
  }

  & .selectorContent {
    & button {
      margin: 0.5rem;
      padding: 0.5rem;
      background-color: ${(props) => props.theme.buttonBg};
      display: inline-block;
      text-decoration: none;
      border: ${(props) => props.theme.buttonBorder};
      color: ${(props) => props.theme.buttonText};
      transition-duration: 0.2s;
      transition-property: background-color, color;
      border-radius: 0.33rem;
    }

    & :hover {
      background-color: ${(props) => props.theme.buttonBgHover};
      color: ${(props) => props.theme.buttonTextHover};
    }

    & .active {
      background-color: grey;
    }
  }
`;

const buttonList = [
  { value: "GDP", text: "GDP" },
  { value: "TEMP", text: "Temperature" },
];

const ChartSelector = (props) => {
  const theme = useTheme();

  const toggleChartView = (e) => {
    props.setChartView(e);
  };

  return (
    <ChartSelectorWrapper theme={theme}>
      <div className="countryNameDiv">
        <CountrySelect className="countryDropdown" />
      </div>
      <div className="selectorContent">
        {buttonList.map((i) => (
          <button
            value={i.value}
            onClick={() => toggleChartView(i.value)}
            className={i.value === props.currentChartView ? "active" : ""}
            key={i.value}
          >
            {i.text}
          </button>
        ))}
      </div>
    </ChartSelectorWrapper>
  );
};

const mapStateToProps = (state) => {
  return {
    currentChartView: state.CountryReducer.currentChartView,
    selectedCountry: state.CountryReducer.selectedCountry,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setChartView: (e) => {
      dispatch(Actions.setChartView(e));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChartSelector);
