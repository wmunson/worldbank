import React from "react";
import styled from "@emotion/styled";
import { useTheme } from "@emotion/react";

const HeaderWrapper = styled.div`
  background-color: ${(props) => props.theme.layerTwo};
  padding: 0.15rem;

  & h1 {
    width: 70%;
    margin: 0.5rem auto 0.5rem auto;
  }
`;

const Header = () => {
  const theme = useTheme();
  return (
    <HeaderWrapper theme={theme}>
      <h1>World Bank Data</h1>
    </HeaderWrapper>
  );
};

export default Header;
