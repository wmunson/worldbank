import React from "react";
import styled from "@emotion/styled";

import CountrySelect from "../countrySelect/CountrySelect";

const SelectBlockWrapper = styled.div`
  position: absolute;
  transition: height 2s;
  background-color: grey;
  padding: 2rem;
  border-radius: 0.25rem;

  & h3 {
    margin-top: 0;
  }
`;

const SelectBlock = (props) => {
  return (
    <SelectBlockWrapper>
      <h3>Select another country</h3>
      <CountrySelect />
    </SelectBlockWrapper>
  );
};

export default SelectBlock;
