import React from "react";
import Select from "react-select";
import { connect } from "react-redux";
import styled from "@emotion/styled";

import Actions from "../../Actions";

const SelectWrapper = styled.div`
  min-width: 15rem;
`;

const CountrySelect = (props) => {
  const handleSelect = (e) => {
    props.setCountry(e);
  };

  return (
    <SelectWrapper>
      <Select
        options={props.countryList}
        onChange={handleSelect}
        defaultValue={props.selectedCountry}
      />
    </SelectWrapper>
  );
};

const mapStateToProps = (state) => {
  return {
    fetching: state.CountryReducer.fetching,
    error: state.CountryReducer.error,
    countryList: state.CountryReducer.countryList,
    selectedCountry: state.CountryReducer.selectedCountry,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCountry: (e) => {
      dispatch(Actions.setCountry(e));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CountrySelect);
