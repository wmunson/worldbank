import * as types from "./ActionTypes";

export const setCountry = (input) => {
  return (dispatch) => {
    dispatch({ type: types.SET_COUNTRY, payload: input });
  };
};

export const setChartView = (input) => {
  return (dispatch) => {
    dispatch({ type: types.SET_CHART_VIEW, payload: input });
  };
};
