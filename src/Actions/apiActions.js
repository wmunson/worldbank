import * as types from "./ActionTypes";

// pagination station - chain api calls to fetch paginated data
const paginatedApiCall = (url, data, resolve, reject) => {
  fetch(url)
    .then((res) => res.json())
    .then((res) => {
      const retrievedData = data.concat(res[1]);
      if (res[0].page < res[0].pages) {
        // TODO: refactor this abomination
        let newUrl = url.split("&page=")[0];
        // eslint-disable-next-line
        newUrl = newUrl + "&page=" + `${res[0].page + 1}`;
        paginatedApiCall(newUrl, retrievedData, resolve, reject);
      } else {
        resolve(retrievedData);
      }
    })
    .catch((err) => {
      reject(err);
    });
};

// getCountryList - fetch list of all countries
export function getCountryList() {
  return (dispatch) => {
    new Promise((resolve, reject) => {
      paginatedApiCall(
        "http://api.worldbank.org/v2/country?format=json",
        [],
        resolve,
        reject
      );
    })
      .then((response) => {
        // immediately truncate response
        let formatted = [];
        for (let item of response) {
          if (item.latitude) {
            formatted.push({ value: item.id, label: item.name });
          }
        }
        dispatch({ type: types.GET_COUNTRIES.SUCCESS, payload: formatted });
      })
      .catch((error) => {
        dispatch({ type: types.GET_COUNTRIES.FAILURE, payload: error });
      });
  };
}

// getGDP - fetch GDP of given country
export function getGDP(countryCode) {
  return (dispatch) => {
    dispatch({ type: types.GET_GDP.REQUEST });

    new Promise((resolve, reject) => {
      paginatedApiCall(
        `http://api.worldbank.org/v2/country/${countryCode}/indicator/NY.GDP.MKTP.CD?format=json`,
        [],
        resolve,
        reject
      );
    })
      .then((res) => {
        dispatch({ type: types.GET_GDP.SUCCESS, payload: res });
      })
      .catch((err) => {
        console.log(err);
        dispatch({ type: types.GET_GDP.FAILURE, payload: err });
      });
  };
}

// getTemperature - fetch avg temperature of given country
export const getTemperature = (countryCode) => {
  return (dispatch) => {
    dispatch({ type: types.GET_TEMPERATURE.REQUEST });

    fetch(
      `http://climatedataapi.worldbank.org/climateweb/rest/v1/country/cru/tas/year/${countryCode}.json`
    )
      .then((response) => response.json())
      .then((response) => {
        console.log("TEMPERATURE RESPONSE", response);
        dispatch({ type: types.GET_TEMPERATURE.SUCCESS, payload: response });
      })
      .catch((error) => {
        dispatch({ type: types.GET_TEMPERATURE.FAILURE, payload: error });
      });
  };
};
