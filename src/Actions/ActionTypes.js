const requestTypes = ["REQUEST", "SUCCESS", "FAILURE"];

function createRequestTypes(base) {
  let res = {};
  requestTypes.forEach((type) => (res[type] = `${base}_${type}`));
  console.log(res);
  return res;
}

export const GET_COUNTRIES = createRequestTypes("GET_COUNTRIES");
export const GET_GDP = createRequestTypes("GET_GDP");
export const GET_TEMPERATURE = createRequestTypes("GET_TEMPERATURE");

// app actions
export const SET_COUNTRY = "SET_COUNTRY";
export const SET_CHART_VIEW = "SET_CHART_VIEW";
