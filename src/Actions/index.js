import * as ApiActions from "./apiActions";
import * as AppActions from "./appActions";

export default Object.assign({}, ApiActions, AppActions);
