import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { useTheme } from "@emotion/react";
import styled from "@emotion/styled";

import Actions from "./Actions";
import CountryView from "./pages/countryView/CountryView";
import Landing from "./pages/landing/Landing";
// import Landing from "./components/landing/Landing";

const RootContainer = (props) => {
  const theme = useTheme();

  useEffect(() => {
    if (!props.countryList) {
      props.getCountryList();
    }
  });

  const WrapperTheme = styled.div`
    background-color: ${theme.background};
    height: 100vh;

    & h1,
    h2,
    i {
      color: ${theme.text};
    }
  `;

  return (
    <>
      <BrowserRouter>
        <WrapperTheme>
          <Switch>
            <Route path="/" exact component={Landing} />
            <Route path="/country" exact component={CountryView} />
          </Switch>
        </WrapperTheme>
      </BrowserRouter>
      {/* <Landing /> */}
      {/* <h1>Root here</h1>
      <Select options={props.countryList} onChange={handleSelect} />
      {props.selectedCountry && <CountryView />} */}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCountryList: () => {
      dispatch(Actions.getCountryList());
    },
    setCountry: (country) => {
      dispatch(Actions.setCountry(country));
    },
  };
};

const mapStateToProps = (state) => {
  return {
    fetching: state.CountryReducer.fetching,
    error: state.CountryReducer.error,
    countryList: state.CountryReducer.countryList,
    selectedCountry: state.CountryReducer.selectedCountry,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
