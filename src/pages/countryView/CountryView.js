import React from "react";
import { connect } from "react-redux";
import styled from "@emotion/styled";

import Header from "../../components/header/Header";
import Actions from "../../Actions";
import ChartContainer from "../../components/chartContainer/ChartContainer";

const CountryViewWrapper = styled.div`
  width: 70%;
  margin: 0 auto 0 auto;
  padding: 1rem auto auto auto;

  & h2 {
    margin-top: 0;
  }
`;

const CountryView = () => {
  return (
    <>
      <Header />
      <CountryViewWrapper>
        <ChartContainer />
      </CountryViewWrapper>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    fetching: state.CountryReducer.fetching,
    error: state.CountryReducer.error,
    countryList: state.CountryReducer.countryList,
    selectedCountry: state.CountryReducer.selectedCountry,
    grossDomesticData: state.CountryReducer.grossDomesticData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGDP: (country) => {
      dispatch(Actions.getGDP(country));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CountryView);
